import os
import tensorflow as tf
from tensorflow import keras

# import the data from resources/train and resources/y_train
DATASET_ROOT = os.path.join(os.getcwd(), "../resources")
NOISY_SUBFOLDER = "train"
DENOISED_SUBFOLDER = "y_train"
DATASET_NOISY_PATH = os.path.join(DATASET_ROOT, NOISY_SUBFOLDER)
DATASET_DENOISED_PATH = os.path.join(DATASET_ROOT, DENOISED_SUBFOLDER)
# Debug, make sure the paths are correct
print("Root directory: ", DATASET_ROOT)
print("Path for noisy input data:", DATASET_NOISY_PATH)
print("Path for denoised comparison data:", DATASET_DENOISED_PATH)


def load_sample(file_path):
    smpl, sampling_rate = tf.audio.decode_wav(
        tf.io.read_file(file_path), desired_channels=1
    )
    if sampling_rate == 16000:
        slices = int(smpl.shape[0] / 16000)
        smpl = tf.split(smpl[: slices * 16000], slices)
        return smpl
    else:
        print("Message bitrate incorrect! Sampling rate for {} is {}, not 16000".format(file_path, sampling_rate))
        return None


npaths = []
for r, d, f in os.walk(DATASET_NOISY_PATH):
    for item in f:
        if '.wav' in item:
            npaths.append(os.path.join(r, item))

for item in npaths:
    print("Importing input data file ", item)
print("Completed load of {} input files!".format(len(npaths)))

nsamples = []
for path in npaths:
    nsample = load_sample(path)
    if nsample:
        nsamples.extend(nsample)
nsamples = tf.stack(nsamples)
print("{} input files split into {} samples!".format(
    len(npaths), nsamples.shape[0]
))

dpaths = []
for r, d, f in os.walk(DATASET_DENOISED_PATH):
    for item in f:
        if '.wav' in item:
            dpaths.append(os.path.join(r, item))

for item in dpaths:
    print("Importing comparison data file ", item)
print("Completed load of {} comparison files!".format(len(dpaths)))

dsamples = []
for path in dpaths:
    dsample = load_sample(path)
    if dsample:
        dsamples.extend(dsample)
dsamples = tf.stack(dsamples)
print("{} comparison files split into {} samples!".format(
    len(dpaths), dsamples.shape[0]
))
